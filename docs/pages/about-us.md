---
title: About
description: Open, Transparent and Trusted Federated Learning for Real-world Healthcare Applications 
---

![fedbiomed-logo](../assets/img/fedbiomed-logo.png#img-sm)

# About us

Federated learning for Healtcare

## What is Fed-BioMed?

Fed-BioMed is an open-source research and development initiative aiming at translating federated learning (FL) into real-world medical research applications.

Fed-BioMed provides:

- A demonstrated framework for deploying federated learning in hospital networks,
- Easy deployment of state-of-the art federated learning methods,
- User-friendly tools for data managment and client participation to federated learning,
- A framework-agnostic environment for easily deploying machine learning methods,
- Clear solutions compliant with data providers' privacy, and nodes governance requirements.

Fed-BioMed is an ongoing initiative, and the code is available on [GitLab](https://gitlab.inria.fr/fedbiomed/fedbiomed).



## Contributors:

Fed-BioMed software has been founded by the French Computer Science Institute INRIA. Contributors are coming from public research as well as companies (involved in a public -  private partnership).
*Coming Soon !*
